module Main where

import           Control.Monad (forever, when)
import           Data.Char     (toLower)
import           Data.List     (intersperse)
import           Data.Maybe    (isJust)
import           System.Exit   (exitSuccess)
import           System.IO     (BufferMode (NoBuffering), hSetBuffering, stdout)
import           System.Random (randomRIO)


main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  word <- pickRandomGameWord
  runGame $ freshPuzzle (fmap toLower word)

type WordList = [String]

allWords :: IO WordList
allWords = do
  dict <- readFile "data/dict.txt"
  return (lines dict)

minWordLength :: Int
minWordLength = 5

maxWordLength :: Int
maxWordLength = 9

validWord :: String -> Bool
validWord w =
  let
    l = length w
  in
    l >= minWordLength && l < maxWordLength

gameWords :: IO WordList
gameWords =
  filter validWord <$> allWords

pickRandom :: [a] -> IO a
pickRandom xs = do
    randomIndex <- randomRIO (0, length xs - 1 )
    return $ xs !! randomIndex

pickRandomGameWord :: IO String
pickRandomGameWord = gameWords >>= pickRandom

data Puzzle = Puzzle {
  solution     :: Solution
  , discovered :: Discovered
  , guessed    :: Guessed
}

newtype Solution = Solution String deriving Show
newtype Discovered = Discovered [Maybe Char] deriving Show
newtype Guessed = Guessed [Char] deriving Show

instance Show Puzzle where
  show (Puzzle _ (Discovered discovered) (Guessed guessed)) =
    intersperse ' ' (fmap renderPuzzleChar discovered)
    ++ "\nGuessed so far: " ++ guessed

renderPuzzleChar :: Maybe Char -> Char
renderPuzzleChar (Just c) = c
renderPuzzleChar _        = '_'

freshPuzzle :: String -> Puzzle
freshPuzzle solution =
  Puzzle
  (Solution solution)
  (Discovered $ replicate (length solution) Nothing)
  (Guessed [])

charInWord :: Puzzle -> Char -> Bool
charInWord (Puzzle (Solution solution) _ _) guess =
  guess `elem` solution

alreadyGuessed :: Puzzle -> Char -> Bool
alreadyGuessed (Puzzle _ _ (Guessed guessed)) guess =
  guess `elem` guessed

fillInCharacter :: Puzzle -> Char -> Puzzle
fillInCharacter (Puzzle (Solution solution) (Discovered discovered) (Guessed guessed)) guess =
  Puzzle
  (Solution solution)
  (Discovered updated)
  (Guessed $ guess : guessed)

  where
    zipper :: Char -> Char -> Maybe Char -> Maybe Char
    zipper guessed wordChar guessChar =
      if wordChar == guessed
      then Just wordChar
      else guessChar

    updated =
      zipWith (zipper guess) solution discovered

handleGuess :: Puzzle -> Char -> IO Puzzle
handleGuess puzzle guess = do
  putStrLn $ "Your guess was: " ++ [guess]
  case (charInWord puzzle guess, alreadyGuessed puzzle guess) of
    (_, True) -> do
      putStrLn "You already guessed that! Pick something else!"
      return puzzle

    (True, _) -> do
      putStrLn "You got it!"
      return (fillInCharacter puzzle guess)

    (False, _) -> do
      putStrLn "Nope, try again!"
      return (fillInCharacter puzzle guess)

gameOver :: Puzzle -> IO ()
gameOver puzzle =
  Control.Monad.when (countWrongGuesses puzzle > 7) $
    do putStrLn "You lose!"
       putStrLn $ "The word was: " ++ show (solution puzzle)
       exitSuccess

countWrongGuesses :: Puzzle -> Int
countWrongGuesses (Puzzle _ (Discovered discovered) (Guessed guessed)) =
  sum $ map (\c -> if Just c `elem` discovered then 0 else 1) guessed

gameWin :: Puzzle -> IO ()
gameWin (Puzzle _ (Discovered discovered) _) =
  Control.Monad.when (all isJust discovered) $
    do putStrLn "You win!"
       exitSuccess

runGame :: Puzzle -> IO ()
runGame puzzle = forever $ do
  gameOver puzzle
  gameWin puzzle
  putStrLn $
    "Current puzzle is: " ++ show puzzle
  putStrLn "Guess a letter: "
  guess <- getLine
  case guess of
    [c] -> handleGuess puzzle c >>= runGame
    _   -> putStrLn "Your guess must be a single character."
