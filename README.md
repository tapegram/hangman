# hangman
Haskell implementation of a hangman cli game, from the haskell book.

## Setup
If you don't have haskell/ghc installed (this includes stack):
https://www.haskell.org/platform/

If you already got it, just get stack!
https://docs.haskellstack.org/en/stable/install_and_upgrade/

##
`stack build` will create an executable
`stack exec hangman` will run it!
```
~/p/hangman> stack exec hangman
Current puzzle is: _ _ _ _ _ _ _ _
Guessed so far: 
Guess a letter: 
```